/*
 * arm_svm.c
 *
 *  Created on: 21 de jun de 2019
 *      Author: rebeca
 */


#include "arm_svm.h"
#include "stm32f4xx.h"


volatile float32_t kernel[N_SV];

uint32_t tempo;
uint32_t millis(void) ;
void systickInit(uint32_t frequency);
static volatile uint32_t ticks;

void KernelRBF(float32_t * X)
{
	uint16_t i;
	float32_t temp[N_FEATURES];

	for(i=0;i<N_SV;i++)
	{
		arm_sub_f32(X,&SV[i*N_FEATURES],&temp[0],N_FEATURES);
		arm_power_f32(&temp[0],N_FEATURES,&kernel[i]);
		kernel[i]=exp(kernel[i]*(-GAMMA));
	}

}


char SVMPredict(float32_t * Pattern)
{
	float32_t a;
	float32_t b;
	char i;
	char j;
	char votes[N_CLASSES]={0};
	char counter=0;

	//Calculate kernels for all support vectors
	tempo=ticks;
	KernelRBF(Pattern);
	tempo=ticks-tempo;
	tempo=ticks;
	//calculate: sum(a_p * k(x_p, x)) between every 2 classes
	for(i=0;i<N_CLASSES;i++)
	{
		for(j=i+1;j<N_CLASSES;j++)
		{
			arm_dot_prod_f32(&DualCoef[(j-1)*N_SV+StartIdx[i]],&kernel[StartIdx[i]],N_SVperClass[i],&a);
			arm_dot_prod_f32(&DualCoef[i*N_SV+StartIdx[j]],&kernel[StartIdx[j]],N_SVperClass[j],&b);
			a=a+b+Intercept[counter];
			if(a>0)
				votes[i]=votes[i]+1;
			else
				votes[j]=votes[j]+1;
			counter++;
		}
	}

	counter=MaxIdx(&votes[0],N_CLASSES);
	tempo=ticks-tempo;
	return counter;
}


char MaxIdx(char * In, char Length)
{
	char c;
	char maximum = *(In);
	char Idx=0;
	for (c = 1; c < Length; c++)
	  {
	    if (*(In+c) > maximum)
	    {
	       maximum  = *(In+c);
	       Idx = c;
	    }
	  }
	return Idx;
}


void systickInit(uint32_t frequency) {
	uint32_t ret;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	ret=SysTick_Config(RCC_Clocks.HCLK_Frequency / frequency);
}


void SysTick_Handler(void) {
	ticks++;
}

// return the system clock as milliseconds

inline uint32_t millis(void) {
	return ticks;
}

