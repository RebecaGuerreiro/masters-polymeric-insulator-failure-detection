
#include "arm_math.h"

//Function prototypes
float32_t MeanAD(float32_t * Input, int Length, float32_t * tempArray, 	float32_t Mean);
float32_t Mean(float32_t * Input, int Length);
float32_t quick_select_median(float32_t * Input, int n, float32_t * tempArray);
float32_t MedianAD(float32_t * Input, int Length, float32_t NormConst,float32_t * tempArray);
float32_t Std(float32_t * Input, int Length);
float32_t Skewness(float32_t * Input, int Length, float32_t * tempArray, float32_t Mean, float32_t Std);
float32_t Kurtosis(float32_t * Input, int Length, float32_t * tempArray, float32_t Mean, float32_t Std);
