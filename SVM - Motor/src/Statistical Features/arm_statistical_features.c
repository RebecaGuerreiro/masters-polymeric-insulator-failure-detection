#include "arm_statistical_features.h"


#ifndef ELEM_SWAP(a,b)
#define ELEM_SWAP(a,b) { register float32_t t=(a);(a)=(b);(b)=t; }
#endif

float32_t Mean(float32_t * Input, int Length)
{
	float32_t mean;

	arm_mean_f32(Input, Length, &mean);

	return mean;
}


float32_t Std(float32_t * Input, int Length)
{
	float32_t std;

	arm_std_f32(Input, Length,&std);

	return std;
}


float32_t MeanAD(float32_t * Input, int Length, float32_t * tempArray, 	float32_t Mean)
{
	float32_t meanAD;

	arm_offset_f32(Input,-Mean,tempArray,Length);

	arm_abs_f32(tempArray,tempArray,Length);

	arm_mean_f32(tempArray,Length,&meanAD);

	return meanAD;
}


float32_t quick_select_median(float32_t * Input, int n, float32_t * tempArray)
{

	float32_t * arr=tempArray;

	arm_copy_f32(Input,arr,n);

	uint16_t low, high ;
	    uint16_t median;
	    uint16_t middle, ll, hh;
	    low = 0 ; high = n-1 ; median = (low + high) / 2;
	    for (;;) {
	    if (high <= low) /* One element only */
	    return arr[median] ;
	    if (high == low + 1) { /* Two elements only */
	    if (arr[low] > arr[high])
	    ELEM_SWAP(arr[low], arr[high]) ;
	    return arr[median] ;
	    }
	    /* Find median of low, middle and high items; swap into position low */
	    middle = (low + high) / 2;
	    if (arr[middle] > arr[high])
	    ELEM_SWAP(arr[middle], arr[high]) ;
	    if (arr[low] > arr[high])
	    ELEM_SWAP(arr[low], arr[high]) ;
	    if (arr[middle] > arr[low])
	    ELEM_SWAP(arr[middle], arr[low]) ;
	    /* Swap low item (now in position middle) into position (low+1) */
	    ELEM_SWAP(arr[middle], arr[low+1]) ;
	    /* Nibble from each end towards middle, swapping items when stuck */
	    ll = low + 1;
	    hh = high;
	    for (;;) {
	    do ll++; while (arr[low] > arr[ll]) ;
	    do hh--; while (arr[hh] > arr[low]) ;
	    if (hh < ll)
	    break;
	    ELEM_SWAP(arr[ll], arr[hh]) ;
	    }
	    /* Swap middle item (in position low) back into correct position */
	    ELEM_SWAP(arr[low], arr[hh]) ;
	    /* Re-set active partition */
	    if (hh <= median)
	    low = ll;
	    if (hh >= median)
	    high = hh - 1;
	    }
	    return arr[median];
}

float32_t MedianAD(float32_t * Input, int Length, float32_t NormConst,float32_t * tempArray)
{
	float32_t median;

	median=quick_select_median(Input, Length, tempArray);

	median=-median;

	arm_offset_f32(Input,median,tempArray,Length);

	arm_abs_f32(tempArray,tempArray,Length);

	median=quick_select_median(tempArray, Length, tempArray);

	return median/ NormConst;
}

float32_t Skewness(float32_t * Input, int Length, float32_t * tempArray, float32_t Mean, float32_t Std)
{
	float32_t skewness;

	float32_t * tempArray2=tempArray+Length;

	arm_offset_f32(Input,-Mean,tempArray,Length);

	arm_scale_f32(tempArray,1/Std,tempArray,Length);

	arm_mult_f32(tempArray,tempArray,tempArray2,Length);

	arm_mult_f32(tempArray2,tempArray,tempArray2,Length);

	arm_mean_f32(tempArray2,Length,&skewness);

	return skewness;
}

float32_t Kurtosis(float32_t * Input, int Length, float32_t * tempArray, float32_t Mean, float32_t Std)
{
	float32_t kurtosis;

	float32_t * tempArray2=tempArray+Length;

	arm_offset_f32(Input,-Mean,tempArray,Length);

	arm_scale_f32(tempArray,1/Std,tempArray,Length);

	arm_mult_f32(tempArray,tempArray,tempArray,Length);

	arm_mult_f32(tempArray,tempArray,tempArray,Length);

	arm_mean_f32(tempArray,Length,&kurtosis);

	return (kurtosis-3);
}

