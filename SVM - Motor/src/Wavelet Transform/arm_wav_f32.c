#include "arm_wav_f32.h"
#include "wavelet_bank.h"

void qmf_wrev(const float32_t *in, int N, float32_t *out)
{

    float32_t *sigOutTemp;
    sigOutTemp = (float32_t*)malloc(N*sizeof(float32_t));

    qmf_even(in, N, sigOutTemp);
    copy_reverse(sigOutTemp, N, out);

    free(sigOutTemp);
    return;
}



void qmf_even(const float32_t *in, int N,float32_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
    {
        out[count] = in[N - count - 1];
        if (count % 2 != 0)
        {
            out[count] = -1 * out[count];
        }
    }
}


void copy_reverse(const float32_t *in, int N,float32_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
        out[count] = in[N - count - 1];
}


//Calculate the decomposition filter coefficients in reverse order
void filtCoef(const char* name, Wavelet_Object * Wav)
{

#ifdef DB1
		Wav->Length=2;
		static float32_t HiD[2];
		//Calculating LoD
		Wav->LoD=db1;
		//Calculation HiD
		qmf_even(db1,Wav->Length,HiD);
		Wav->HiD=HiD;

#endif
#ifdef DB2

			Wav->Length=4;
			static float32_t HiD[4];
			//Calculating LoD
			Wav->LoD=db2;
			//Calculation HiD
			qmf_even(db2,Wav->Length,HiD);
			Wav->HiD=HiD;


#endif
#ifdef DB4
					Wav->Length=8;
					static float32_t HiD[8];
					//Calculating LoD
					Wav->LoD=db4;
					//Calculation HiD
					qmf_even(db4,Wav->Length,HiD);
					Wav->HiD=HiD;

#endif

#ifdef DB10
		Wav->Length=20;
		static float32_t HiD[20];
		//Calculating LoD
		Wav->LoD=db10;
		//Calculation HiD
		qmf_even(db10,Wav->Length,HiD);
		Wav->HiD=HiD;

#endif

}
//Return the filter reconstrution coeffients in reverse order.
//TODO: Testar
void filtReconstCoef(const char* name, Wavelet_Object * Wav)
{

#ifdef DB1
		Wav->Length=2;
		static float32_t HiR[2];
		static float32_t LoR[2];
		//Calculating LoR
		copy_reverse(db1,Wav->Length,LoR);
		Wav->LoR=LoR;
		//Calculation HiR
		qmf_rev(db1,Wav->Length,HiR);
		Wav->HiR=HiR;
#endif


#ifdef DB2
			Wav->Length=4;
			static float32_t HiR[4];
			static float32_t LoR[4];
			//Calculating LoR
			copy_reverse(db2,Wav->Length,LoR);
			Wav->LoR=LoR;
			//Calculation HiR
			qmf_rev(db2,Wav->Length,HiR);
			Wav->HiR=HiR;

#endif
#ifdef DB4
		Wav->Length=8;
		static float32_t HiR[8];
		static float32_t LoR[8];
		//Calculating LoR
		copy_reverse(db4,Wav->Length,LoR);
		Wav->LoR=LoR;
		//Calculation HiR
		qmf_rev(db4,Wav->Length,HiR);
		Wav->HiR=HiR;

#endif
}

//Discrete Wavelet Transform.
//Obs: Filter coefficients should be in reverse order.
void WaveletTransform(float32_t * in,  float32_t * Output, unsigned int SignalLength, int Levels,const Wavelet_Object * Wav, unsigned int * BlockSize)
{
	arm_status ret;

	int i;
	int j;
	int n;

	arm_fir_decimate_instance_f32 SHiD;
	arm_fir_decimate_instance_f32 SLoD;

	static float32_t * firState1;
	static float32_t * firState2;

	float32_t * Aprox;
	float32_t * Detail;

	int NumBlocks;

	unsigned int BlockSize_l;


	Aprox=Output;
	Detail=Output+SignalLength/2;

	BlockSize_l=*BlockSize;

	NumBlocks=SignalLength/BlockSize_l;

	n=SignalLength;

	for(j=0;j<Levels;j++)
	{

		firState1=(float32_t*)malloc((BlockSize_l+Wav->Length-1)*sizeof(float32_t));
		firState2=(float32_t*)malloc((BlockSize_l+Wav->Length-1)*sizeof(float32_t));


		ret=arm_fir_decimate_init_f32(&SHiD,Wav->Length,2,Wav->HiD,firState1,BlockSize_l);
		ret=arm_fir_decimate_init_f32(&SLoD,Wav->Length,2,Wav->LoD,firState2,BlockSize_l);


		for(i=0;i<NumBlocks;i++)
		{
			//Obtain Detail
			arm_fir_decimate_f32(&SHiD,in+(i*BlockSize_l+1),Detail+(i*BlockSize_l/2),BlockSize_l);
			//Obtain Approximation
			arm_fir_decimate_f32(&SLoD,in+(i*BlockSize_l+1),Aprox+(i*BlockSize_l/2),BlockSize_l);
		}

		BlockSize_l=*(BlockSize+j+1);
		n=n/2;
		NumBlocks=n/BlockSize_l;
		if((n%BlockSize_l)!=0)
			NumBlocks++;
		Detail=Output+n/2;

		//Copy approximation coeficients to input;
		arm_copy_f32(Aprox,in,n);

		free(firState1);
		free(firState2);

	}


}

void WaveletGetCoefPosition(unsigned int * CoefPosition, int WaveResultLength, int levels)
{
	int i;

	for(i=levels;i>0;i--)
	{

		if((WaveResultLength%2)==0)
			WaveResultLength=WaveResultLength/2;
		else
			WaveResultLength=WaveResultLength/2+1;

		*(CoefPosition+i)=WaveResultLength;

	}
	*(CoefPosition+i)=WaveResultLength;
	*(CoefPosition)=0;
}

void WaveletEnergy(float32_t * WaveResult, int * CoefPosition, float32_t * EnergyPER, int levels, int Length)
{
	float32_t Et=0;
	float32_t E=0;
	int i;
	int Position;
	int N;

	for(i=0;i<levels;i++)
	{
		Position=*(CoefPosition+i);
		N=*(CoefPosition+i+1)-Position-1;
		arm_power_f32(WaveResult+Position,N,&E);
		*(EnergyPER+i)=E;
		Et=Et+E;
	}
	Position=*(CoefPosition+i);
	N=Length-Position;
	arm_power_f32(WaveResult+Position,N,&E);
	*(EnergyPER+i)=E;
	Et=Et+E;

	arm_scale_f32(EnergyPER,1/Et,EnergyPER,levels+1);


}


