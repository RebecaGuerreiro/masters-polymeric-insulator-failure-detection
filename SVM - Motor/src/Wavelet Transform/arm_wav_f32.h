

#include "arm_math.h"

typedef struct Wavelet_Object
{
	float32_t * HiD;
	float32_t * LoD;
	float32_t * HiR;
	float32_t * LoR;
	unsigned int Length;

}Wavelet_Object;


void WaveletTransform(float32_t * in,  float32_t * Output, unsigned int SignalLength, int Levels,const Wavelet_Object * Wav, unsigned int * BlockSize);
void filtCoef(const char* name, Wavelet_Object * Wav);
void copy_reverse(const float32_t *in, int N,float32_t *out);
void qmf_even(const float32_t *in, int N,float32_t *out);
void qmf_wrev(const float32_t *in, int N, float32_t *out);
void filtReconstCoef(const char* name, Wavelet_Object * Wav);
void WaveletGetCoefPosition(unsigned int * CoefPosition, int WaveResultLength, int levels);
void WaveletEnergy(float32_t * WaveResult, int * CoefPosition, float32_t * EnergyPER, int levels, int Length);
