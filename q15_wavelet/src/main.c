
/* Includes */
#include "stm32f4xx.h"
#include "arm_math.h"
#include <Wavelet Transform/arm_wav_q15.h>
#include <stdio.h>
#include <math.h>


#define TEST_LENGTH_SAMPLES  5000
#define BLOCK_SIZE            8
#define LEVELS				  5
#define N_ATRIBUTOS			  10
#define MAX_STRLEN 5000 // this is the maximum string length of our string in characters


static int CoefPosition[LEVELS+1];
static float32_t Atributos[N_ATRIBUTOS];
static float EnergyPER[LEVELS+1];


volatile int temp[2];
volatile int countTemp=0;
volatile int cnt = 0; // this counter is used to determine the string length
static q15_t testOutput[MAX_STRLEN];
static float32_t testOutput_f[MAX_STRLEN];




void ConfigureUsart(int baudrate);
void SendData(USART_TypeDef* USARTx, volatile char *s);
void ConfigLED(void);
void sendQ15Array(q15_t * Array, unsigned int Length);
float32_t MeanAD(float32_t * Input, int Length);
uint32_t millis(void) ;
void systickInit(uint32_t frequency);


q15_t testInput_q15[MAX_STRLEN];
uint32_t tempo=0;


static volatile uint32_t ticks;

int main(void)
{
  int i = 0;
  q15_t media=0;


  //Config Led
  ConfigLED();

  //Config Systick
  systickInit(1000000);

  //Config Serial
  ConfigureUsart(115200);
  //SendData(USART1, "Conex�o estabelecida\r\n");

  //tempo=ticks;
  static Wavelet_Object Wav;

  filtCoef("db4", &Wav);
  //tempo=ticks-tempo;

  while (1)
  {

	  while(cnt<MAX_STRLEN);

	    //Escalonamento e remo��o da m�dia

	  //  tempo=ticks;
	    //arm_scale_q15(testInput_q15, 32767, 3, testInput_q15, MAX_STRLEN);

	    arm_shift_q15(&testInput_q15,3,&testInput_q15, MAX_STRLEN);
	    //tempo=ticks-tempo;

	  //  tempo=ticks;
	    arm_mean_q15(&testInput_q15,MAX_STRLEN,&media);

	    arm_negate_q15(&media,&media,1);

	    arm_offset_q15(&testInput_q15,media,&testInput_q15,MAX_STRLEN);

	  //  tempo=ticks-tempo;

	    //tempo=ticks;
	    WaveletTransform(&testInput_q15[0], &testOutput[0],MAX_STRLEN, LEVELS,&Wav,BLOCK_SIZE);
	    //tempo=ticks-tempo;

	    //tempo=ticks;
	    WaveletGetCoefPosition(&CoefPosition, TEST_LENGTH_SAMPLES,LEVELS);
	    //tempo=ticks-tempo;

	    //Remove offset da Aproxima��o
	   // tempo=ticks;
	    arm_mean_q15(&testOutput,CoefPosition[0],&media);

	    arm_negate_q15(&media,&media,1);


	    arm_offset_q15(&testOutput,media,&testOutput,CoefPosition[0]);
	   // tempo=ticks-tempo;

	    //"Converter" para float e escalonar de volta
	    //tempo=ticks;
	    for(i=0; i<TEST_LENGTH_SAMPLES;i++)
	    {
	  	  testOutput_f[i]=(float32_t)testOutput[i]/8;
	    }
	    //tempo=ticks-tempo;

	    //sendFloatArray(testOutput_f,TEST_LENGTH_SAMPLES);

	    //tempo=ticks;
	    WaveletEnergy(&testOutput_f[0], &CoefPosition[0], EnergyPER, LEVELS, TEST_LENGTH_SAMPLES);


	    //Obter atibutos
	    //MeanAD(D5)
	    Atributos[0]= MeanAD(&testOutput_f[0]+CoefPosition[1]+4, CoefPosition[1]-4);

	    //EnergyPER(A5)
	    Atributos[1]=EnergyPER[0];

	    //EnergyPER(D3)
	    Atributos[2]= EnergyPER[3];

	    //Std(D3)
	    arm_std_f32(&testOutput_f[0]+CoefPosition[3]+4,CoefPosition[3]-4,&Atributos[3]);

	    //EnergyPER(D5)
	    Atributos[4]= EnergyPER[1];

	    //Std(D4)
	    arm_std_f32(&testOutput_f[0]+CoefPosition[2]+4,CoefPosition[2]-4,&Atributos[5]);

	    //MeanAD(A5)
	    Atributos[6]= MeanAD(&testOutput_f[0]+4, CoefPosition[0]-4);

	    //EnergyPER(D1)
	    Atributos[7]= EnergyPER[5];

	    //Std(A5)
	    arm_std_f32(&testOutput_f[0]+4,CoefPosition[0]-4,&Atributos[8]);

	    //MeanAD(D4)
	    Atributos[9]= MeanAD(&testOutput_f[0]+CoefPosition[2]+4, CoefPosition[2]-4);

	    //tempo=ticks-tempo;

	    //sendFloatArray(&Atributos,N_ATRIBUTOS);
	    sendFloatArray(&testOutput_f[0]+4,CoefPosition[0]-4);
	    sendFloatArray(&testOutput_f[0]+CoefPosition[1]+4,CoefPosition[1]-4);
	    sendFloatArray(&testOutput_f[0]+CoefPosition[2]+4,CoefPosition[2]-4);
	    sendFloatArray(&testOutput_f[0]+CoefPosition[3]+4,CoefPosition[3]-4);
	    sendFloatArray(&testOutput_f[0]+CoefPosition[4]+4,CoefPosition[4]-4);
	    sendFloatArray(&testOutput_f[0]+CoefPosition[5]+4,CoefPosition[5]-4);

	    SendData(USART1, "\n");
	    cnt=0;
	    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt
  }
  return 0;
}


void ConfigureUsart(int baudrate){
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1); //
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART1, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting


	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART1, ENABLE);

}


//writes out a string to the passed in usart. The string is passed as a pointer
void SendData(USART_TypeDef* USARTx, volatile char *s){

	while(*s){
		// wait until data register is empty
		while( !(USARTx->SR & 0x00000040) );
		USART_SendData(USARTx, *s);
		*s++;
	}
}

void ConfigLED(void){

	GPIO_InitTypeDef GPIO_LED;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	  GPIO_LED.GPIO_Pin = GPIO_Pin_12;
	  GPIO_LED.GPIO_Mode = GPIO_Mode_OUT;
	  GPIO_LED.GPIO_OType = GPIO_OType_PP;
	  GPIO_LED.GPIO_Speed = GPIO_Speed_50MHz;

	  GPIO_Init(GPIOD,&GPIO_LED);

	  GPIO_WriteBit(GPIOD, GPIO_Pin_12, Bit_SET);

}

void sendQ15Array(q15_t * Array, unsigned int Length)
{
	int i;
	char c[20]={0};
	for(i=0;i<Length; i++)
	{
		  sprintf(c, "%d,", Array[i]);
		  SendData(USART1, c);
	}
}

void sendFloatArray(float32_t * Array, unsigned int Length)
{
	int i;
	char c[20]={0};
	for(i=0;i<Length; i++)
	{
		  sprintf(c, "%1.7f,", Array[i]);
		  SendData(USART1, c);
	}
}

void USART1_IRQHandler(void){

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART1, USART_IT_RXNE) ){


		q15_t t = USART1->DR; // the character from the USART1 data register is saved in t

		if( (countTemp!= 2) && (cnt < MAX_STRLEN) ){
			temp[countTemp] = (q15_t)t;
			countTemp++;
		}
		if((countTemp== 2) && (cnt < MAX_STRLEN))
		{
			countTemp=0;
			int a=temp[0]<<8;
			int b=temp[1];
			testInput_q15[cnt]=(q15_t)a+b;
			cnt++;

		}
		if(cnt==MAX_STRLEN)
		{
			//cnt = 0;
			USART_ITConfig(USART1, USART_IT_RXNE, DISABLE); // disable the USART1 receive interrupt
		}
	}
}


float32_t MeanAD(float32_t * Input, int Length)
{
	float32_t mean;
	float32_t testInput_f32[TEST_LENGTH_SAMPLES];

	arm_mean_f32(Input, Length, &mean);

	mean=-mean;

	arm_offset_f32(Input,mean,&testInput_f32,Length);

	arm_abs_f32(&testInput_f32[0],&testInput_f32[0],Length);

	arm_mean_f32(&testInput_f32[0],Length,&mean);

	return mean;
}

void systickInit(uint32_t frequency) {
	uint32_t ret;
  RCC_ClocksTypeDef RCC_Clocks;
  RCC_GetClocksFreq(&RCC_Clocks);
  ret=SysTick_Config(RCC_Clocks.HCLK_Frequency / frequency);
}


void SysTick_Handler(void) {
  ticks++;
}

// return the system clock as milliseconds

//inline uint32_t millis(void) {
//  return ticks;
//}

