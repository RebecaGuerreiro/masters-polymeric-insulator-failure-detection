


#include <Wavelet Transform/arm_wav_q15.h>
#include "wavelet_bank.h"

#define MAX_LENGTH 5000
#define MAX_BLOCK_SIZE	170//(filter+blocklength)
void qmf_wrev(const q15_t *in, int N, q15_t *out)
{

	q15_t *sigOutTemp;
    sigOutTemp = (q15_t*)malloc(N*sizeof(q15_t));

    qmf_even(in, N, sigOutTemp);
    copy_reverse(sigOutTemp, N, out);

    free(sigOutTemp);
    return;
}



void qmf_even(const q15_t *in, int N,q15_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
    {
        out[count] = in[N - count - 1];
        if (count % 2 != 0)
        {
            out[count] = -1 * out[count];
        }
    }
}


void copy_reverse(const q15_t *in, int N,q15_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
        out[count] = in[N - count - 1];
}


//Calculate the decomposition filter coefficients in reverse order
void filtCoef(const char* name, Wavelet_Object * Wav)
{

//	if (!strcmp(name,"haar") || !strcmp(name,"db1"))
//	{
//		Wav->Length=2;
//		static q15_t HiD[2];
		//Calculating LoD
//		Wav->LoD=db2;
		//Calculation HiD
//		qmf_even(db2,Wav->Length,HiD);
//		Wav->HiD=HiD;

//	}

//	else if (!strcmp(name,"db2"))
//		{
//			Wav->Length=4;
//			static q15_t HiD[4];
			//Calculating LoD
//			Wav->LoD=db2;
			//Calculation HiD
//			qmf_even(db2,Wav->Length,HiD);
//			Wav->HiD=HiD;
//}
		if (!strcmp(name,"db4"))
			{
			Wav->Length=8;
			static q15_t HiD[8];
				//Calculating LoD
				Wav->LoD=db4;
			//Calculation HiD
				qmf_even(db4,Wav->Length,HiD);
				Wav->HiD=HiD;

			}
	if (!strcmp(name,"db10"))
	{
		Wav->Length=20;
		static q15_t HiD[20];
		//Calculating LoD
		Wav->LoD=db10;
		//Calculation HiD
		qmf_even(db10,Wav->Length,HiD);
		Wav->HiD=HiD;
	}

}
//Return the filter reconstrution coeffients in reverse order.
//TODO: Testar
void filtReconstCoef(const char* name, Wavelet_Object * Wav)
{
//	if (!strcmp(name,"haar") || !strcmp(name,"db1"))
//	{
//		Wav->Length=2;
//		static q15_t HiR[2];
//		static q15_t LoR[2];
		//Calculating LoR
//		copy_reverse(db2,Wav->Length,LoR);
//		Wav->LoR=LoR;
		//Calculation HiR
//		qmf_rev(db2,Wav->Length,HiR);
//		Wav->HiR=HiR;

//	}

//	else if (!strcmp(name,"db2"))
//		{
//			Wav->Length=4;
//			static q15_t HiR[4];
//			static q15_t LoR[4];
			//Calculating LoR
//			copy_reverse(db2,Wav->Length,LoR);
//			Wav->LoR=LoR;
			//Calculation HiR
//			qmf_rev(db2,Wav->Length,HiR);
//			Wav->HiR=HiR;

//		}

//		if (!strcmp(name,"db4"))
//	{
//		Wav->Length=8;
//		static q15_t HiR[8];
//		static q15_t LoR[8];
		//Calculating LoR
//		copy_reverse(db4,Wav->Length,LoR);
//		Wav->LoR=LoR;
		//Calculation HiR
//		qmf_rev(db4,Wav->Length,HiR);
//		Wav->HiR=HiR;
//	}

}

//Discrete Wavelet Transform.
//Obs: Filter coefficients should be in reverse order.
void WaveletTransform(q15_t * in,  q15_t * Output, unsigned int SignalLength, int Levels,const Wavelet_Object * Wav, unsigned int BlockSize)
{
	arm_status ret;

	int i;
	int j;
	int n;

	arm_fir_decimate_instance_q15 SHiD;
	arm_fir_decimate_instance_q15 SLoD;

	q15_t  firState1[MAX_BLOCK_SIZE];
	q15_t  firState2[MAX_BLOCK_SIZE];

	q15_t * Aprox;
	q15_t * Detail;

	int NumBlocks;

	q15_t temp[MAX_LENGTH];

	//firState1=(q15_t*)malloc((BlockSize+Wav->Length-1)*sizeof(q15_t));
	//firState2=(q15_t*)malloc((BlockSize+Wav->Length-1)*sizeof(q15_t));


	//temp=(q15_t*)malloc(SignalLength*sizeof(q15_t));

	Aprox=in;
	Detail=Output+SignalLength/2;

	arm_copy_q15(in,temp,SignalLength);

	NumBlocks=SignalLength/BlockSize;

	n=SignalLength;

	for(j=0;j<Levels;j++)
	{
		//Copy approximation coeficients to temp;
		arm_copy_q15(Aprox,temp,n);
		Aprox=Output;

		ret=arm_fir_decimate_init_q15(&SHiD,Wav->Length,2,Wav->HiD,firState1,BlockSize);
		ret=arm_fir_decimate_init_q15(&SLoD,Wav->Length,2,Wav->LoD,firState2,BlockSize);

		for(i=0;i<NumBlocks;i++)
		{
			//Obtain Detail
			arm_fir_decimate_fast_q15(&SHiD,temp+(i*BlockSize),Detail+(i*BlockSize/2),BlockSize);
			//Obtain Approximation
			arm_fir_decimate_fast_q15(&SLoD,temp+(i*BlockSize),Aprox+(i*BlockSize/2),BlockSize);
		}

		n=n/2;
		NumBlocks=n/BlockSize;
		if((n%BlockSize)!=0)
			NumBlocks++;
		Detail=Output+n/2;



	}

	//free(firState1);
	//free(firState2);
	//free(temp);

}

void WaveletGetCoefPosition(int * CoefPosition, int WaveResultLength, int levels)
{
	int i;

	for(i=levels;i>0;i--)
	{
		WaveResultLength=WaveResultLength/2;
		*(CoefPosition+i)=WaveResultLength;

	}
	*(CoefPosition+i)=WaveResultLength;

}

//TODO: fazer versao q15
void WaveletEnergy(float32_t * WaveResult, int * CoefPosition, float32_t * EnergyPER, int levels, int Length)
{
	float32_t Et=0;
	float32_t E=0;
	int i;
	int Position;

	//Energia parcial
	Position=*(CoefPosition+1);
	arm_power_f32(WaveResult+4,Position-4,&E);

	*(EnergyPER)=E;

	for(i=1;i<levels+1;i++)
	{
		Position=*(CoefPosition+i);
		arm_power_f32(WaveResult+Position+4,Position-4,&E);
		*(EnergyPER+i)=E;
		Et=Et+E;
	}

	arm_scale_f32(EnergyPER,100/Et,EnergyPER,levels+1);


}
