

#include "arm_math.h"

typedef struct Wavelet_Object
{
	q15_t * HiD;
	q15_t * LoD;
	q15_t * HiR;
	q15_t * LoR;
	unsigned int Length;

}Wavelet_Object;


void WaveletTransform(q15_t * in,  q15_t * Output, unsigned int SignalLength, int Levels,const Wavelet_Object * Wav, unsigned int BlockSize);
void filtCoef(const char* name, Wavelet_Object * Wav);
void copy_reverse(const q15_t *in, int N,q15_t *out);
void qmf_even(const q15_t *in, int N, q15_t *out);
void qmf_wrev(const q15_t *in, int N, q15_t *out);
void filtReconstCoef(const char* name, Wavelet_Object * Wav);
void WaveletGetCoefPosition(int * CoefPosition, int WaveResultLength, int levels);
void WaveletEnergy(float32_t* WaveResult, int * CoefPosition, float32_t * EnergyPER, int levels, int Length);
