#include "arm_math.h"

#define N_ATRIBUTOS		27

float32_t GetPosterior(const float32_t *in, const float32_t * mean, const float32_t * variance, uint8_t N,float32_t ProbClass);
//float32_t CalculateProbDensity(float32_t mean, float32_t variance, float32_t feature);
uint8_t Predict(float32_t *PosteriorVec,uint8_t N);
