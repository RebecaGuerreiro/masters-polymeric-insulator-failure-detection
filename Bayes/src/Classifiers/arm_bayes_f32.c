#include "arm_bayes_f32.h"

uint8_t Predict(float32_t *PosteriorVec,uint8_t N)
{
	uint32_t * pIndex;
	float32_t * pValue;

	arm_max_f32(PosteriorVec,N,&pValue,&pIndex);

	return pIndex;
}


float32_t GetPosterior(const float32_t *in, const float32_t * mean, const float32_t * variance, uint8_t N, float32_t ProbClass)
{

	float32_t diff[N_ATRIBUTOS];
	float32_t varianceScaled1[N_ATRIBUTOS];
	float32_t varianceScaled2[N_ATRIBUTOS];
	float32_t out[N_ATRIBUTOS];
	float32_t posterior=1;
	float32_t  SQRT=0;

	arm_sub_f32(in,mean,diff,N);

	arm_mult_f32(diff,diff,diff,N);

	arm_scale_f32(variance,2,varianceScaled1,N);

	arm_scale_f32(varianceScaled1,3.14159265359,varianceScaled2,N);

	for(int i=0;i<N;i++)
	{
		//*(out+i)=1/sqrt(*(varianceScaled2+i))*exp(-*(diff+i) / *(varianceScaled1+i));

		arm_sqrt_f32(varianceScaled2[i],&SQRT);

		*(out+i)=1/(SQRT)*exp(-*(diff+i) / *(varianceScaled1+i));

		posterior=posterior*out[i];
	}

	posterior=posterior*ProbClass;

	return posterior;


}

//float32_t CalculateProbDensity(float32_t mean, float32_t variance, float32_t feature)
//{
//	float32_t p;

//	float32_t diff=feature-mean;

//	p=1/sqrt(6.28*variance)*exp(-(diff*diff)/(2*variance));

//	return p;

//}
