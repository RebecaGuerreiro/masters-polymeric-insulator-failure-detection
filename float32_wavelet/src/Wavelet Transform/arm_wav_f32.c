


#include "arm_wav_f32.h"
#include "wavelet_bank.h"

void qmf_wrev(const float32_t *in, int N, float32_t *out)
{

    float32_t *sigOutTemp;
    sigOutTemp = (float32_t*)malloc(N*sizeof(float32_t));

    qmf_even(in, N, sigOutTemp);
    copy_reverse(sigOutTemp, N, out);

    free(sigOutTemp);
    return;
}



void qmf_even(const float32_t *in, int N,float32_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
    {
        out[count] = in[N - count - 1];
        if (count % 2 != 0)
        {
            out[count] = -1 * out[count];
        }
    }
}


void copy_reverse(const float32_t *in, int N,float32_t *out)
{
    int count = 0;
    for (count = 0; count < N; count++)
        out[count] = in[N - count - 1];
}


//Calculate the decomposition filter coefficients in reverse order
void filtCoef(const char* name, Wavelet_Object * Wav)
{

//	if (!strcmp(name,"haar") || !strcmp(name,"db1"))
//	{
//		Wav->Length=2;
//		static float32_t HiD[2];
//		//Calculating LoD
//		Wav->LoD=db2;
//		//Calculation HiD
//		qmf_even(db2,Wav->Length,HiD);
//		Wav->HiD=HiD;

//	}

//	else if (!strcmp(name,"db2"))
//		{
//			Wav->Length=4;
//			static float32_t HiD[4];
//			//Calculating LoD
//			Wav->LoD=db2;
//			//Calculation HiD
//			qmf_even(db2,Wav->Length,HiD);
//			Wav->HiD=HiD;

//		}
	if (!strcmp(name,"db4"))
				{
					Wav->Length=8;
					static float32_t HiD[8];
					//Calculating LoD
					Wav->LoD=db4;
					//Calculation HiD
					qmf_even(db4,Wav->Length,HiD);
					Wav->HiD=HiD;

				}
	if (!strcmp(name,"db10"))
	{
		Wav->Length=20;
		static float32_t HiD[20];
		//Calculating LoD
		Wav->LoD=db10;
		//Calculation HiD
		qmf_even(db10,Wav->Length,HiD);
		Wav->HiD=HiD;
	}


}
//Return the filter reconstrution coeffients in reverse order.
//TODO: Testar
void filtReconstCoef(const char* name, Wavelet_Object * Wav)
{
//	if (!strcmp(name,"haar") || !strcmp(name,"db1"))
//	{
//		Wav->Length=2;
//		static float32_t HiR[2];
//		static float32_t LoR[2];
//		//Calculating LoR
//		copy_reverse(db2,Wav->Length,LoR);
//		Wav->LoR=LoR;
//		//Calculation HiR
//		qmf_rev(db2,Wav->Length,HiR);
//		Wav->HiR=HiR;

//	}

//	else if (!strcmp(name,"db2"))
//		{
//			Wav->Length=4;
//			static float32_t HiR[4];
//			static float32_t LoR[4];
//			//Calculating LoR
//			copy_reverse(db2,Wav->Length,LoR);
//			Wav->LoR=LoR;
//			//Calculation HiR
//			qmf_rev(db2,Wav->Length,HiR);
//			Wav->HiR=HiR;

//		}
	if (!strcmp(name,"db4"))
	{
		Wav->Length=8;
		static float32_t HiR[8];
		static float32_t LoR[8];
		//Calculating LoR
		copy_reverse(db4,Wav->Length,LoR);
		Wav->LoR=LoR;
		//Calculation HiR
		qmf_rev(db4,Wav->Length,HiR);
		Wav->HiR=HiR;
	}

}

//Discrete Wavelet Transform.
//Obs: Filter coefficients should be in reverse order.
void WaveletTransform(float32_t * in,  float32_t * Output, unsigned int SignalLength, int Levels,const Wavelet_Object * Wav, unsigned int BlockSize)
{
	arm_status ret;

	int i;
	int j;
	int n;

	arm_fir_decimate_instance_f32 SHiD;
	arm_fir_decimate_instance_f32 SLoD;

	static float32_t * firState1;
	static float32_t * firState2;

	float32_t * Aprox;
	float32_t * Detail;

	int NumBlocks;

	float32_t * temp;

	firState1=(float32_t*)malloc((BlockSize+Wav->Length-1)*sizeof(float32_t));
	firState2=(float32_t*)malloc((BlockSize+Wav->Length-1)*sizeof(float32_t));

	temp=(float32_t*)malloc((SignalLength)*sizeof(float32_t));

	Aprox=in;
	Detail=Output+SignalLength/2;


	arm_copy_f32(in,temp,SignalLength);

	NumBlocks=SignalLength/BlockSize;

	n=SignalLength;

	for(j=0;j<Levels;j++)
	{
		//Copy approximation coeficients to temp;
		arm_copy_f32(Aprox,temp,n);
		Aprox=Output;

		ret=arm_fir_decimate_init_f32(&SHiD,Wav->Length,2,Wav->HiD,firState1,BlockSize);
		ret=arm_fir_decimate_init_f32(&SLoD,Wav->Length,2,Wav->LoD,firState2,BlockSize);


		for(i=0;i<NumBlocks;i++)
		{
			//Obtain Detail
			arm_fir_decimate_f32(&SHiD,temp+(i*BlockSize),Detail+(i*BlockSize/2),BlockSize);
			//Obtain Approximation
			arm_fir_decimate_f32(&SLoD,temp+(i*BlockSize),Aprox+(i*BlockSize/2),BlockSize);
		}

		n=n/2;
		NumBlocks=n/BlockSize;
		if((n%BlockSize)!=0)
			NumBlocks++;
		Detail=Output+n/2;



	}

	free(firState1);
	free(firState2);
	free(temp);


}

void WaveletGetCoefPosition(int * CoefPosition, int WaveResultLength, int levels)
{
	int i;

	for(i=levels;i>0;i--)
	{
		WaveResultLength=WaveResultLength/2;
		*(CoefPosition+i)=WaveResultLength;

	}
	*(CoefPosition+i)=WaveResultLength;

}

void WaveletEnergy(float32_t * WaveResult, int * CoefPosition, float32_t * EnergyPER, int levels, int Length)
{
	float32_t Et=0;
	float32_t E=0;
	int i;
	int Position;

	//Energia parcial
	Position=*(CoefPosition+1);
	arm_power_f32(WaveResult+4,Position-4,&E);

	*(EnergyPER)=E;

	for(i=1;i<levels+1;i++)
	{
		Position=*(CoefPosition+i);
		arm_power_f32(WaveResult+Position+4,Position-4,&E);
		*(EnergyPER+i)=E;
		Et=Et+E;
	}

	arm_scale_f32(EnergyPER,100/Et,EnergyPER,levels+1);


}

