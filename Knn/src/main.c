
/* Includes */
#include "stm32f4xx.h"
#include "arm_math.h"
#include <stdio.h>
#include <stdlib.h>
#include "arm_knn.h"
//#include "arm_wav_f32.h"
//#include "wavelet_bank.h"

#define MAX_TEMP_BUFF	15
#define MAX_STRLEN	10
#define TEST_LENGTH_SAMPLES  10
#define BLOCK_SIZE            8
#define LEVELS				  5
#define N_ATRIBUTOS			  N_FEATURES
//#define N_ATRIBUTOS 10
//#define MEDIR_TEMPO

//volatile float32_t testInput_f32[TEST_LENGTH_SAMPLES];
//volatile float32_t in[MAX_STRLEN];
//static float32_t testOutput[TEST_LENGTH_SAMPLES+(BLOCK_SIZE-1)*2];
//static int CoefPosition[LEVELS+1];
volatile float32_t Atributos[N_ATRIBUTOS];
//static float EnergyPER[LEVELS+1];


//Recep��o serial
//volatile int temp[MAX_TEMP_BUFF]; //Recep��o de inteiros. Dados brutos para pr�-processamento
volatile char temp[MAX_TEMP_BUFF];
volatile int countTemp=0;
volatile int cnt = 0; // this counter is used to determine the string length

//Prototipos de fun��o
//void ConfigureUsart(int baudrate);
//void SendData(USART_TypeDef* USARTx, volatile char *s);
void ConfigLED(void);
//void sendFloatArray(float32_t * Array, unsigned int Length);
//float32_t MeanAD(float32_t * Input, int Length);

//Medi��o de tempo
#ifdef MEDIR_TEMPO
uint32_t millis(void) ;
void systickInit(uint32_t frequency);
static volatile uint32_t ticks;
uint32_t tempo;
#endif

//Normaliza��o dos dados

#if N_FEATURES==27
const float32_t Min[N_ATRIBUTOS]={0.00072709,4.0977,13.584,2.5,1.2832,0.99586,0.7232,0.47891,0.0030218,12.275,0.11248,0.39698,1.9903,4.3633,0.6578500000000001,9.8109,3.2294,77.15300000000001,27.191,0.29106,1.5753,32.146,17.919,48.681,1.4307,9.853400000000001,9.860300000000001};
const float32_t Delta_Inv[N_ATRIBUTOS]={16.39857457030405,0.03173766912210433,0.02322286988226005,0.0392156862745098,0.1285082759329701,0.4274527237287556,0.1375723974741708,0.5549112419468506,13.73113342267724,0.02115864753924929,1.504121292341014,0.3343900057515081,0.09930782446348947,0.02049192925366345,0.1329089108779298,0.1092168062821507,0.1105876628403335,0.1190476190476192,0.01029876723756166,1.040539415633064,0.06554067782169004,0.02108414682999852,0.03788883416057289,0.005509065166731857,0.04589197945875,0.06525881646610457,0.05932007331961062};
#endif

#if N_FEATURES==20
const float32_t Min[N_ATRIBUTOS]={27.1910000000000,	0.657850000000000,	1.28320000000000,	4.09770000000000,	13.5840000000000,	48.6810000000000,	12.2750000000000,	17.9190000000000,	0.478910000000000,	0.995860000000000,	3.22940000000000,	9.85340000000000,	32.1460000000000,	9.86030000000000,	0.000727090000000000,	9.81090000000000,	77.1530000000000,	1.99030000000000,	0.112480000000000,	0.00302180000000000};
const float32_t Delta_Inv[N_ATRIBUTOS]={0.0102987672375617,	0.132908910877930,	0.128508275932970,	0.0317376691221043,	0.0232228698822600,	0.00550906516673186,	0.0211586475392493,	0.0378888341605729,	0.554911241946851,	0.427452723728756,	0.110587662840334,	0.0652588164661046,	0.0210841468299985,	0.0593200733196106,	16.3985745703041,	0.109216806282151,	0.11904761904761,	0.0993078244634895,	1.50412129234101,	13.7311334226772};
#endif

#if N_FEATURES==10
const float32_t Min[N_ATRIBUTOS]={0.982920000000000,	0.000727310000000000,	1.96440000000000,	13.5680000000000,	0.00285330000000000,	4.09650000000000,	0.482000000000000,	9.81150000000000,	0.660010000000000,	3.22580000000000};
const float32_t Delta_Inv[N_ATRIBUTOS]={0.425752944081608,	15.9256742412889,	0.0991021346599806,	0.0231953980330302,	19.4621820609283,	0.0316801571335794,	0.560381059120202,	0.109188185838292,	0.132543550497105,	0.110348480501423};
#endif

int main(void)
{

  //float32_t media;

  //Config Serial
  //ConfigureUsart(115200);


  //Config Systick
#ifdef MEDIR_TEMPO
   systickInit(1000000);
#endif

  //tempo=ticks;
  //static Wavelet_Object Wav;
  //filtCoef("db4", &Wav);
  //tempo=ticks-tempo;


  while (1)
  {

	  //Aguarda recep��o serial
	    while(cnt<MAX_STRLEN);

	    //Remo��o da m�dia
	    //tempo=ticks;
	    //arm_mean_f32(&testInput_f32[0],TEST_LENGTH_SAMPLES,&media);

	    //arm_negate_f32(&media,&media,1);

	    //arm_offset_f32(&testInput_f32[0],media,&testInput_f32[0],TEST_LENGTH_SAMPLES);
	    //tempo=ticks-tempo;

	    //tempo=ticks;
	    //WaveletTransform(&testInput_f32[0], &testOutput[0],TEST_LENGTH_SAMPLES, LEVELS,&Wav,BLOCK_SIZE);
	    //tempo=ticks-tempo;

	    //tempo=ticks;
	    //WaveletGetCoefPosition(&CoefPosition[0], TEST_LENGTH_SAMPLES,LEVELS);
	    //tempo=ticks-tempo;

	    //tempo=ticks;
	    //WaveletEnergy(&testOutput[0], &CoefPosition[0], EnergyPER, LEVELS, TEST_LENGTH_SAMPLES);

	    //Obter atibutos
	    //MeanAD(D5)
	    //Atributos[0]= MeanAD(&testOutput[0]+CoefPosition[1]+4, CoefPosition[1]-4);

	    //EnergyPER(A5)
	    //Atributos[1]=EnergyPER[0];

	    //EnergyPER(D3)
	    //Atributos[2]= EnergyPER[3];

	    //Std(D3)
	    //arm_std_f32(&testOutput[0]+CoefPosition[3]+4,CoefPosition[3]-4,&Atributos[3]);

	    //EnergyPER(D5)
	    //Atributos[4]= EnergyPER[1];

	    //Std(D4)
	    //arm_std_f32(&testOutput[0]+CoefPosition[2]+4,CoefPosition[2]-4,&Atributos[5]);

	    //MeanAD(A5)
	    //Atributos[6]= MeanAD(&testOutput[0]+4, CoefPosition[0]-4);

	    //EnergyPER(D1)
	    //Atributos[7]= EnergyPER[5];

	    //Std(A5)
	    //arm_std_f32(&testOutput[0]+4,CoefPosition[0]-4,&Atributos[8]);

	    //MeanAD(D4)
	    //Atributos[9]= MeanAD(&testOutput[0]+CoefPosition[2]+4, CoefPosition[2]-4);

	    // tempo=ticks-tempo;


	    //tempo=ticks;
	    //Normaliza��o
	    arm_sub_f32(&Atributos[0],&Min[0],&Atributos[0],N_ATRIBUTOS);

	    arm_mult_f32(&Atributos[0],&Delta_Inv[0],&Atributos[0],N_ATRIBUTOS);

	    //tempo=ticks-tempo;

	    //tempo=ticks;

	    //Classifica��o
	    char Prediction=knnTest(&Atributos[0]);
	    //tempo=ticks-tempo;


	    //USART_SendData(USART1,Prediction+48);
	    //SendData(USART1,"\n");

	    //cnt=0;
	    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // disable the USART1 receive interrupt



  }
  return 0;
}


void ConfigureUsart(int baudrate){
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART1 initialization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PB6 for TX and PB7 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7; // Pins 6 (TX) and 7 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOB, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1); //
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART1, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting


	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART1, ENABLE);

}


//writes out a string to the passed in usart. The string is passed as a pointer
void SendData(USART_TypeDef* USARTx, volatile char *s){

	while(*s){
		// wait until data register is empty
		while( !(USARTx->SR & 0x00000040) );
		USART_SendData(USARTx, *s);
		*s++;
	}
}

void ConfigLED(void){

	GPIO_InitTypeDef GPIO_LED;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

	  GPIO_LED.GPIO_Pin = GPIO_Pin_12;
	  GPIO_LED.GPIO_Mode = GPIO_Mode_OUT;
	  GPIO_LED.GPIO_OType = GPIO_OType_PP;
	  GPIO_LED.GPIO_Speed = GPIO_Speed_50MHz;

	  GPIO_Init(GPIOD,&GPIO_LED);

	  GPIO_WriteBit(GPIOD, GPIO_Pin_12, Bit_SET);

}

//void sendFloatArray(float32_t * Array, unsigned int Length)
//{
//	int i;
//	char c[20]={0};
//	for(i=0;i<Length; i++)
//	{
//		  sprintf(c, "%1.7f,", Array[i]);
//		  SendData(USART1, c);
//	}
//}

void USART1_IRQHandler(void){

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART1, USART_IT_RXNE) ){



#if MAX_TEMP_BUFF==2 //Inteiros de 2 bytes

		int t = USART1->DR; // the character from the USART1 data register is saved in t

		if( (countTemp!= MAX_TEMP_BUFF) && (cnt < MAX_STRLEN) ){
			temp[countTemp] = (int)t;
			countTemp++;
		}


		if((countTemp== MAX_TEMP_BUFF) && (cnt < MAX_STRLEN))
		{
			countTemp=0;
			int a=temp[0]<<8;
			int b=temp[1];
			testInput_f32[cnt]=(float32_t)a+b;
			cnt++;

		}
#endif

#if MAX_TEMP_BUFF>2 //array de char para compor float

		char t = USART1->DR; // the character from the USART1 data register is saved in t

		if( (countTemp!= MAX_TEMP_BUFF) && (cnt < MAX_STRLEN) ){
					temp[countTemp] = (char)t;
					countTemp++;
				}

		if((temp[countTemp-1]=='\n') && (cnt < MAX_STRLEN))
				{
					temp[countTemp-1]='\0';
					Atributos[cnt]=(float32_t)atof(temp);
					countTemp=0;
					cnt++;
				}

#endif
		if(cnt==MAX_STRLEN)
		{
			//cnt = 0;
			USART_ITConfig(USART1, USART_IT_RXNE, DISABLE); // disable the USART1 receive interrupt
		}
	}
}

//float32_t MeanAD(float32_t * Input, int Length)
//{
//	float32_t mean;

//	arm_mean_f32(Input, Length, &mean);

//	mean=-mean;

//	arm_offset_f32(Input,mean,&testInput_f32[0],Length);

//	arm_abs_f32(&testInput_f32[0],&testInput_f32[0],Length);

//	arm_mean_f32(&testInput_f32[0],Length,&mean);

//	return mean;
//}

#ifdef MEDIR_TEMPO

void systickInit(uint32_t frequency) {
	uint32_t ret;
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	ret=SysTick_Config(RCC_Clocks.HCLK_Frequency / frequency);
}


void SysTick_Handler(void) {
	ticks++;
}

// return the system clock as milliseconds

inline uint32_t millis(void) {
	return ticks;
}

#endif

