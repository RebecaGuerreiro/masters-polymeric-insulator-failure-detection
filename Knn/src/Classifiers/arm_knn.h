#include "arm_math.h"


#define N_FEATURES				10
#define N_SAMPLES_TRAINING		1168
#define N_NEIGHBORS 			5
#define N_CLASSES 				5

float32_t SquaredEuclidianDistance(float32_t * p,const float32_t * q, uint8_t n);
char knnTest(float32_t * testIn);


